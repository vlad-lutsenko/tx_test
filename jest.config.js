module.exports = {
  projects: [
    '<rootDir>/apps/reactapp',
    '<rootDir>/apps/nodeapp',
    '<rootDir>/apps/expressapp',
  ],
};
