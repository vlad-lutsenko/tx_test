#!/bin/bash
set -e
rm -rf /home/ubuntu/tx_test/
git clone https://gitlab.com/vlad-lutsenko/tx_test.git
curl -sL https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.0/install.sh -o install_nvm.sh
bash install_nvm.sh
source /home/ubuntu/.nvm/nvm.sh
command -v nvm
nvm install --lts
npm install pm2 -g
pm2 kill
npm remove pm2 -g
npm install pm2 -g
cd /home/ubuntu/tx_test
npm install -g nx
npm install
npm run build expressapp
cd dist/apps/expressapp
ls -la
pm2 start main.js
pm2 status
