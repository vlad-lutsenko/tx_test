set -e
echo "stack creation start"
aws cloudformation create-stack --stack-name $AWS_STACK_NAME --template-body file://cloudFormationTemplate.json --parameters ParameterKey=KeyName,ParameterValue=test-key-vlad
aws cloudformation wait stack-create-complete --stack-name $AWS_STACK_NAME
echo "stack created"